package com.company.items;

import com.company.Personnage;
import com.company.classes.Barbare;

import java.util.Scanner;

public abstract class arme extends Personnage {

    public double att;
    public double def;
    public double mana;
    public double vie;

    public arme(double att,double vie, double def, double mana) {

        super(att, vie, def, mana);
        this.att = att;
        this.vie = vie;
        this.def = def;
        this.mana = mana;
        this.choisirPseudo();

    }
    public void choisirPseudo(){
        Scanner sc = new Scanner(System.in);
        String reponse;

        do {

            System.out.println("Veuillez saisir un pseudo :");
            this.pseudo = sc.nextLine();
            System.out.println("Vous avez saisi : " + this.pseudo);
            System.out.println("Voulez vous enregistré ces informations(o/n) :");
            reponse = sc.nextLine();

            switch(reponse){
                case "o":
                    System.out.println("Vos informations ont bel et bien été enregistrés");
                    break;
                case "n":

                    break;
                default:
                    System.out.println("Vous devez choisir entre 'o' et 'n' !");
                    break;
            }

        } while(!reponse.equalsIgnoreCase("o"));
    }

    public void attaque(Personnage personnage){
        personnage.vie = personnage.vie - this.att;
        System.out.println(personnage.vie);
    }
    public void afficherStats(){
        System.out.println(att + vie + def + mana);
    }
}
