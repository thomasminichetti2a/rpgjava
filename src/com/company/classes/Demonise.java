/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.classes;

import com.company.Personnage;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Danie
 */


public class Demonise extends Personnage {
    double attMagic;
    double agilite;
    String reponseRace;

    public Demonise(){
        super(15, 350, 30, 400);
        this.attMagic = 35;
        this.agilite = 40;
        this.choisirClasseAgiliteMagic();
    }
    public void discretion(){
        this.agilite = this.agilite * 1.80;
    }
    public void enchanteur(){
        this.attMagic = this.attMagic * 1.25;
    }

    public void choisirClasseAgiliteMagic(){
        Scanner sc = new Scanner(System.in);
        String reponseClasse;

        do {

            System.out.println("Quel sous classe voulez vous ? (affuteur/sacPV/blindé/source/discretion/enchanteur)");
            reponseClasse = sc.nextLine();

            switch(reponseClasse){
                case "affuteur":
                    this.affuteur();
                    System.out.println("attaque : "+att + ", vie : " + vie + ", def : " + def + ", mana : " + mana + ", agilite : " + agilite + ", attaque magique : " + attMagic);
                    break;
                case "sacPV":
                    this.sacPV();
                    System.out.println("attaque : "+att + ", vie : " + vie + ", def : " + def + ", mana : " + mana + ", agilite : " + agilite + ", attaque magique : " + attMagic);
                    break;
                case "blinde":
                    this.blinde();
                    System.out.println("attaque : "+att + ", vie : " + vie + ", def : " + def + ", mana : " + mana + ", agilite : " + agilite + ", attaque magique : " + attMagic);
                    break;
                case "source":
                    this.source();
                    System.out.println("attaque : "+att + ", vie : " + vie + ", def : " + def + ", mana : " + mana + ", agilite : " + agilite + ", attaque magique : " + attMagic);
                    break;
                case "discretion":
                    this.discretion();
                    System.out.println("attaque : "+att + ", vie : " + vie + ", def : " + def + ", mana : " + mana + ", agilite : " + agilite + ", attaque magique : " + attMagic);
                    break;
                case "enchanteur":
                    this.enchanteur();
                    System.out.println("attaque : "+att + ", vie : " + vie + ", def : " + def + ", mana : " + mana + ", agilite : " + agilite + ", attaque magique : " + attMagic);
                    break;

                default:
                    System.out.println("Vous devez choisir une classe !");
                    break;
            }

        } while(!reponseClasse.equalsIgnoreCase("affuteur") && !reponseClasse.equalsIgnoreCase("source") && !reponseClasse.equalsIgnoreCase("blinde") && !reponseClasse.equalsIgnoreCase("sacPV") && !reponseClasse.equalsIgnoreCase("discretion") && !reponseClasse.equalsIgnoreCase("enchanteur"));
    }
    

    public void getStats(){
        System.out.println(attMagic + " " + att + " " + vie + " " + def + " " + mana );
    }


    public void choisirRace() {
        Scanner sc = new Scanner(System.in);

        ArrayList<String> races = new ArrayList<>();
        races.add("Orc");
        races.add("Humain");

        String reponse;

        do {
            for (String race : races) {
                System.out.println(races.indexOf(race) + " - " + race);
            }
            System.out.println("Veuillez saisir le numéro de la race : ");

            this.reponseRace = sc.nextLine();
            System.out.println("Vous avez saisi : " + races.get(Integer.parseInt(this.reponseRace)) );
            System.out.println("Voulez vous enregistré ces informations(o/n) :");
            reponse = sc.nextLine();

            switch(reponse){
                case "o":
                    System.out.println("Vos informations ont bel et bien été enregistrés");
                    break;
                default:
                    System.out.println("Vous devez choisir entre 'o' et 'n' !");
                    break;
            }

        } while(!reponse.equalsIgnoreCase("o"));
    }
}
