/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.classes;

import com.company.Personnage;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Danie
 */
public class Mage extends Personnage {
    double attMagic;
    String reponseRace;

    public Mage(){
        super(5, 400, 30, 350);
        this.attMagic = 50;
        this.choisirClasse();
    }

    public void setStats(String stat, int valeur){
        switch (stat.toString()) {
            case "att":
                this.att = valeur;
                break;
            case "vie":
                this.vie = valeur;
                break;
            case "def":
                this.def = valeur;
                break;
            case "mana":
                this.mana = valeur;
                break;
            case "attMagic":
                this.attMagic = valeur;
                break;
            default:
                System.out.println("Vous n'avez pas entré une stat valide ! (att/vie/def/mana/attMagic");
                break;
        }
    }
    public void enchanteur(){
        this.attMagic = this.attMagic * 1.25;
    }

    public void choisirClasse(){
        Scanner sc = new Scanner(System.in);
        String reponseClasse;

        do {

            System.out.println("Quel sous classe voulez vous ? (affuteur/sacPV/blindé/source/enchanteur)");
            reponseClasse = sc.nextLine();

            switch(reponseClasse){
                case "affuteur":
                    this.affuteur();
                    System.out.println("attaque : "+att + ", vie : " + vie + ", def : " + def + ", mana : " + mana + ", attaque magique : " +attMagic);
                    break;
                case "sacPV":
                    this.sacPV();
                    System.out.println("attaque : "+att + ", vie : " + vie + ", def : " + def + ", mana : " + mana + ", attaque magique : " +attMagic);
                    break;
                case "blinde":
                    this.blinde();
                    System.out.println("attaque : "+att + ", vie : " + vie + ", def : " + def + ", mana : " + mana + ", attaque magique : " +attMagic);
                    break;
                case "source":
                    this.source();
                    System.out.println("attaque : "+att + ", vie : " + vie + ", def : " + def + ", mana : " + mana + ", attaque magique : " +attMagic);
                    break;
                case "enchanteur":
                    this.enchanteur();
                    System.out.println("attaque : "+att + ", vie : " + vie + ", def : " + def + ", mana : " + mana + ", attaque magique : " +attMagic);
                    break;
                default:
                    System.out.println("Vous devez choisir une classe !");
                    break;
            }

        } while(!reponseClasse.equalsIgnoreCase("affuteur") && !reponseClasse.equalsIgnoreCase("source") && !reponseClasse.equalsIgnoreCase("blinde") && !reponseClasse.equalsIgnoreCase("sacPV") && !reponseClasse.equalsIgnoreCase("enchanteur"));
    }

    public void getStats(){
        System.out.println(attMagic + " " + att + " " + vie + " " + def+ " " + mana);
    }

    public void choisirRace() {
        Scanner sc = new Scanner(System.in);

        ArrayList<String> races = new ArrayList<>();
        races.add("Elf");
        races.add("Humain");
        races.add("Orc");

        String reponse;

        do {
            for (String race : races) {
                System.out.println(races.indexOf(race) + " - " + race);
            }
            System.out.println("Veuillez saisir le numéro de la race : ");

            this.reponseRace = sc.nextLine();
            System.out.println("Vous avez saisi : " + races.get(Integer.parseInt(this.reponseRace)) );
            System.out.println("Voulez vous enregistré ces informations(o/n) :");
            reponse = sc.nextLine();

            switch(reponse){
                case "o":
                    System.out.println("Vos informations ont bel et bien été enregistrés");
                    break;
                default:
                    System.out.println("Vous devez choisir entre 'o' et 'n' !");
                    break;
            }

        } while(!reponse.equalsIgnoreCase("o"));
    }
    
}
