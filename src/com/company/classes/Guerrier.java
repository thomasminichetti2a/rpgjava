/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.classes;

import com.company.Personnage;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Danie
 */
public class Guerrier extends Personnage {
    String reponseRace;

    public Guerrier(){
        super(15, 400, 30, 200);
        this.choisirClasse();
    }


    public void getStats(){
        System.out.println(att + " " + vie + " " + def+ " " + mana);
    }


    public void choisirRace() {
        Scanner sc = new Scanner(System.in);

        ArrayList<String> races = new ArrayList<>();
        races.add("Elf");
        races.add("Orc");
        races.add("Humain");
        races.add("Citoyen");
        races.add("Ange");


        String reponse;

        do {
            for (String race : races) {
                System.out.println(races.indexOf(race) + " - " + race);
            }
            System.out.println("Veuillez saisir le numéro de la race : ");

            this.reponseRace = sc.nextLine();
            System.out.println("Vous avez saisi : " + races.get(Integer.parseInt(this.reponseRace)) );
            System.out.println("Voulez vous enregistré ces informations(o/n) :");
            reponse = sc.nextLine();

            switch(reponse){
                case "o":
                    System.out.println("Vos informations ont bel et bien été enregistrés");
                    break;
                default:
                    System.out.println("Vous devez choisir entre 'o' et 'n' !");
                    break;
            }

        } while(!reponse.equalsIgnoreCase("o"));
    }
}

