/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.classes;

import com.company.Personnage;

import java.lang.reflect.Array;
import java.util.Scanner;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Danie
 */
public class Assassin extends Personnage {
   double agilite;
   String reponseRace;

   
    public Assassin(){
        super(15, 400, 30, 150);
        this.agilite = 60;
        //this.choisirClasseAgilite();
        this.choisirRace();
    }

    public void discretion(){
        this.agilite = this.agilite * 1.80;
    }

    public void choisirClasseAgilite(){
        Scanner sc = new Scanner(System.in);
        String reponseClasse;

        do {

            System.out.println("Quel sous classe voulez vous ? (affuteur/sacPV/blindé/source/discretion)");
            reponseClasse = sc.nextLine();

            switch(reponseClasse){
                case "affuteur":
                    this.affuteur();
                    System.out.println("attaque :" +att + ", vie : " + vie + ", def : " + def + ", mana : " + mana + ", agilite : " + agilite);
                    break;
                case "sacPV":
                    this.sacPV();
                    System.out.println("attaque :" +att + ", vie : " + vie + ", def : " + def + ", mana : " + mana + ", agilite : " + agilite);
                    break;
                case "blinde":
                    this.blinde();
                    System.out.println("attaque :" +att + ", vie : " + vie + ", def : " + def + ", mana : " + mana + ", agilite : " + agilite);
                    break;
                case "source":
                    this.source();
                    System.out.println("attaque :" +att + ", vie : " + vie + ", def : " + def + ", mana : " + mana + ", agilite : " + agilite);
                    break;
                case "discretion":
                    this.discretion();
                    System.out.println("attaque :" +att + ", vie : " + vie + ", def : " + def + ", mana : " + mana + ", agilite : " + agilite);
                    break;
                default:
                    System.out.println("Vous devez choisir une classe !");
                    break;
            }

        } while(!reponseClasse.equalsIgnoreCase("affuteur") && !reponseClasse.equalsIgnoreCase("source") && !reponseClasse.equalsIgnoreCase("blinde") && !reponseClasse.equalsIgnoreCase("sacPV") && !reponseClasse.equalsIgnoreCase("discretion"));
    }

    public void getStats(){
        System.out.println(att + " " + vie + " " + def + " " + mana + " " + agilite);
    }

    public void choisirRace() {
        Scanner sc = new Scanner(System.in);
        
        ArrayList<String> races = new ArrayList<>();
        races.add("Démon");
        races.add("Humain");
        races.add("Ange");
        
        String reponse;

        do {
            for (String race : races) {
                System.out.println(races.indexOf(race) + " - " + race);
            }
            System.out.println("Veuillez saisir le numéro de la race : ");
            
            this.reponseRace = sc.nextLine();
            System.out.println("Vous avez saisi : " + races.get(Integer.parseInt(this.reponseRace)) );
            System.out.println("Voulez vous enregistré ces informations(o/n) :");
            reponse = sc.nextLine();

            switch(reponse){
                case "o":
                    System.out.println("Vos informations ont bel et bien été enregistrés");
                    break;
                default:
                    System.out.println("Vous devez choisir entre 'o' et 'n' !");
                    break;
            }

        } while(!reponse.equalsIgnoreCase("o"));
    }
}
