package com.company;

import java.util.Random;
import java.util.Scanner;
import com.company.classes.*;
import com.company.items.armes.Baton;
import com.company.items.armes.Dague;
import com.company.items.armes.Epee;
import com.company.items.armes.Hache;
import com.company.classes.Barbare;

public class Main {




    public static void main(String[] args) {



        //Voleur voleur1 = new Voleur();

        //Guerrier guerrier1 = new Guerrier();

        //Clerc clerc1 = new Clerc();

        //Mage mage1 = new Mage();

        //Assassin assassin1 = new Assassin();

        //Barbare barbare1 = new Barbare();

        //Demonise demonise1 = new Demonise();


        Scanner sc = new Scanner(System.in);
        String reponseClasse;

        do {

            System.out.println("Quel classe voulez vous ? (barbare, guerrier, assassin, voleur, mage, demonise, clerc)");
            reponseClasse = sc.nextLine();

            switch(reponseClasse){
                case "barbare":

                    Barbare barbare1 = new Barbare();
                    
                    break;
                case "guerrier":

                    Guerrier guerrier1 = new Guerrier();
                    break;
                case "assassin":

                    Assassin assassin1 = new Assassin();
                    break;
                case "voleur":

                    Voleur voleur1 = new Voleur();
                    break;
                case "mage":

                    Mage mage1 = new Mage();
                    break;
                case "clerc":

                    Clerc clerc1 = new Clerc();
                    break;
                case "demonise":

                    Demonise demonise1 = new Demonise();
                    break;
                default:
                    System.out.println("Vous devez choisir une classe !");
                    break;
            }

        } while(!reponseClasse.equalsIgnoreCase("barbare") && !reponseClasse.equalsIgnoreCase("guerrier") && !reponseClasse.equalsIgnoreCase("assassin") && !reponseClasse.equalsIgnoreCase("voleur") && !reponseClasse.equalsIgnoreCase("mage") && !reponseClasse.equalsIgnoreCase("clerc") && !reponseClasse.equalsIgnoreCase("demonise"));

        Scanner sn = new Scanner(System.in);
        String reponseArme;

        do {

            System.out.println("Quel arme voulez vous ? (baton/dague/epee/hache)");
            reponseArme = sc.nextLine();

            switch(reponseArme){
                case "baton":

                    Baton baton1 = new Baton();
                    System.out.println("Votre personnage aura donc un baton magique !");
                    break;

                case "dague":

                    Dague dague1 = new Dague();
                    System.out.println("Votre personnage aura donc une dague !");
                    break;

                case "epee":

                    Epee epee1 = new Epee();
                    System.out.println("Votre personnage aura donc une épée !");
                    break;

                case "hache":

                    Hache hache1 = new Hache();
                    System.out.println("Votre personnage aura donc une hache !");
                    break;

                default:

                    System.out.println("Vous devez choisir une arme !");
                    break;
            }


        } while(!reponseArme.equalsIgnoreCase("baton") && !reponseArme.equalsIgnoreCase("dague") && !reponseArme.equalsIgnoreCase("epee") && !reponseArme.equalsIgnoreCase("hache"));

    }

}
