
package com.company;

import com.company.classes.Barbare;

import java.util.Random;
import java.util.Scanner;

public abstract class Personnage {
    
    public double att;
    public double vie;
    public double def;
    public double mana;
    public String pseudo;

    public Personnage(double att, double vie, double def, double mana){
    
        this.att = att;
        this.vie = vie;
        this.def = def;
        this.mana = mana;

    }
    public double getVie(){
        return this.vie;
    }
    public void setStats(String stat, int valeur){
        switch (stat.toString()) {
            case "att":
                this.att = valeur;

                break;
            case "vie":
                this.vie = valeur;

                break;
            case "def":
                this.def = valeur;

                break;
            case "mana":
                this.mana = valeur;

                break;
            default:
                System.out.println("Vous n'avez pas entré une stat valide ! (att/vie/def/mana/attMagic");
                break;
        }
    }


    public void affuteur(){
        this.att = this.att * 1.20;
    }
    public void sacPV(){
        this.vie = this.vie * 1.50;
    }
    public void blinde(){
        this.def = this.def * 1.20;
    }
    public void source(){
        this.mana = this.mana * 2;
    }

    public void choisirClasse(){

        Scanner sc = new Scanner(System.in);
        String reponseClasse;

        do {

            System.out.println("Quel sous classe voulez vous ? (affuteur/sacPV/blindé/source)");
            reponseClasse = sc.nextLine();

            switch(reponseClasse){
                case "affuteur":
                    this.affuteur();
                    System.out.println("attaque : "+att + ", vie : " + vie + ", def : " + def + ", mana : " + mana);
                    break;
                case "sacPV":
                    this.sacPV();
                    System.out.println("attaque : "+att + ", vie : " + vie + ", def : " + def + ", mana : " + mana);
                    break;
                case "blinde":
                    this.blinde();
                    System.out.println("attaque : "+att + ", vie : " + vie + ", def : " + def + ", mana : " + mana);
                    break;
                case "source":
                    this.source();
                    System.out.println("attaque : "+att + ", vie : " + vie + ", def : " + def + ", mana : " + mana);
                    break;

                default:
                    System.out.println("Vous devez choisir une classe !");
                    break;
            }


        } while(!reponseClasse.equalsIgnoreCase("affuteur") && !reponseClasse.equalsIgnoreCase("source") && !reponseClasse.equalsIgnoreCase("blinde") && !reponseClasse.equalsIgnoreCase("sacPV"));

    }



    // parer avec l’épée

    public void isEpeiste(){

        double parer = Math.random();

        if (parer < 0.33) {
            System.out.println("il pare");

        }else{
            System.out.println("il pare pas");
        }
    }

    public void isSorcier(){

    }

    public void isBerserker(){

        double berserk = Math.random();
        if (berserk < 0.33) {

            System.out.println("Il rentre en mode berserk!");
            this.att = this.att * 1.70;

        }
    }

    public void isDagueur(){

    }

    public void isNecromancien(){

    }

}

    

